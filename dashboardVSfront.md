## UI/UX discussion for Kernel CI case ##

#### General Purpose #### 

UX-UI in kernelCI is an important thing and in the future data visualization from logs also will be really important. So we would like to discuss which kind of front-end we want to provide for our users. 
We think that choosing a technology we'd like to adopt is the first steps. This doc is to ask and promote a discussion around the most stable/used front-end technologies. First of all, the points that will guide this doc:  

1. Features that make it stand out.
2. Popularity regarding Github stars and community engagement (an important point in FOSS and to get attention and more contributors).
3. Integration support, ease of implementation and learning curve.
4. Documentation.
5. KernelCI specific requirements.
5. Performance.

Most famous/popular/used frameworks:
* [React](https://reactjs.org/) - [134046 stars on Github](https://github.com/facebook/react) 

* [Vue](https://vuejs.org/) - [145605 stars on Github](https://github.com/vuejs/vue) 

* [Angular](https://angular.io/) - [50169 stars on Github](https://github.com/angular/angular)

* [Backbone](https://backbonejs.org/) - [27524 stars on Github](https://github.com/jashkenas/backbone)


##### Other options that could be considered and are not in JS: #####
* [Elm](https://elm-lang.org/)

* [WASM](https://github.com/rustwasm/)


One detail to be observed, JS is the main language here. It is difficult to find solutions in other languages, Elm seems to be a good comparative tool-set. One of the problems in JS is the type system! I am not sure if we could use typescript with all these JS frameworks, but we can using angular, react or vue. Those are some of the most popular and with the big communities, if you have good points about other don't hesitate to share with us, let's discuss about, even if it is not listed above or in the table below.

It was suggested by teams member to take a look in ssp(server side rendering) technology and it seems to be interesting.  In _kernel-ci_ platform it will help processing logs in server side instead of in the client side, that makes things easier and quicker if you compare with the process of loading data, processing and editing data using jquery, JS scripts and flask in front-end that already has been used, etc. 

Frameworks that could be used in this case:

- [Next.js](https://nextjs.org/)
- [Nuxt.js](https://nuxtjs.org/)


Another interesting thing at this point is consider to use WASM and Rust language to build a general front-end application for _kernel-ci_. Why can it be interesting? Because it will makes possible to work with a static typed language and avoid JS from the application _kernel-ci_ stack. Anyway, looking more details about front-end Rust technologies and resources, it was possible to conclude that it is not exactly ready, it still is more experimental tools. A ideia in this case could be maintaining the AGL features and think about use WASM in the future in order to avoid JS. 

##### Why does _kernel-ci_ need to have a general front-end? #####

In the case of general front-end applications we could think about include features to get new jobs, new trees, new test plans using UI. 

====================================================================


#### Optimized tool for analysis and data visualization (generic dashboard tools) ####

[Software quality dashboard powered by Linaro(SQUAD)](https://github.com/Linaro/squad/) powered by Linaro and being used on [LKFT]( https://lkft.linaro.org/).
 

It is a kind of [dashboard](https://qa-reports.linaro.org/lkft/linux-stable-rc-4.14-oe/metrics/?environment=juno-r2&environment=x15&environment=x86&environment=hi6220-hikey&environment=qemu_x86_64&environment=dragonboard-410c&environment=qemu_arm&environment=qemu_arm64&environment=qemu_i386&environment=i386&environment=&metric=:summary:) and/or quality assurance tool developed by Lirano using python. 


In the use-case of _kernel-ci_ this tool probably will be a challenge because it will require a lot of adjustments to work well. The features that could be found in Grafana and Elasticstack as a pre-set of metrics could be applied and are not defined in SQUAD, se we should apply JSON files to define everything as we can see in [documentation](https://squad.readthedocs.io/en/latest/intro.html#metrics).

In the case of other tools, we could use rich dashboards that will help in task of analyses extracting information from _kernel-ci_ logs. The most famous and used tools are: [Grafana](https://grafana.com/), [Elastic Stack + Kibana](https://www.elastic.co/products/elastic-stack), [BigQuery + DataStudio](https://cloud.google.com/bigquery/docs/visualize-data-studio), [Microsoft Power BI](https://powerbi.microsoft.com/en-us/), [Metabase](https://www.metabase.com/)*.

Kevin started producing an experimental [dashboard](https://datastudio.google.com/reporting/b2d21715-a5f6-4321-89fa-7bca9b99f41e/page/wo91) using Big Query and Datastudio tools. 

For the context of _kernel-ci_ could use any one of the listed above tools. 

\* not exhaustive list, so feel free to add tools that could be helpful in _kernel-ci_ context.